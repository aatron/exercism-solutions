package hamming

import (
	"errors"
)

// Distance gets the hamm distance between two strings with the same length.
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return -1, errors.New("Strings have different lengths")
	}

	jonHamm := 0

	for i := range a {
		if a[i] != b[i] {
			jonHamm++
		}
	}

	return jonHamm, nil
}
