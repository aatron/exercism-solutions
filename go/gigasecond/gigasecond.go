// Package gigasecond has method(s) that manipulate time using 10^9 seconds
package gigasecond

// import path for the time package from the standard library
import (
	"math"
	"time"
)

// AddGigasecond - Add 10^9 seconds to the time passed in and return the value
func AddGigasecond(t time.Time) (newTime time.Time) {
	newTime = t.Add(time.Second * time.Duration(math.Pow10(9)))
	return
}
