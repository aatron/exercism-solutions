// Package acronym determines the acronym from strings being passed in
package acronym

import (
	"strings"
	"unicode"
)

// Determine if the letter will separate the word
var wordSeparator = func(letter rune) bool {
	return !unicode.IsLetter(letter)
}

// Abbreviate turns a string into an acronym.....TLA's FTW
func Abbreviate(s string) (acronym string) {
	words := strings.FieldsFunc(s, wordSeparator)

	for _, word := range words {
		acronym += strings.ToUpper(word[0:1])
	}

	return
}
