// Package bob is a bot teenager that has limited responses to questions. Loosely inspired by every teenager, ever.
package bob

import (
	"strings"
	"unicode"
)

// Hey returns a response to the remark presented. A remark can be a question, command, etc. Warning: responses can be terse.
func Hey(remark string) string {
	remark = strings.TrimSpace(remark)
	isYelling := areYouYelling(remark)
	isQuestion := strings.HasSuffix(remark, "?")

	switch {
	case isYelling && isQuestion:
		return "Calm down, I know what I'm doing!"
	case isYelling:
		return "Whoa, chill out!"
	case isQuestion:
		return "Sure."
	case strings.TrimSpace(remark) == "":
		return "Fine. Be that way!"
	default:
		return "Whatever."
	}
}

func areYouYelling(remark string) bool {
	mightYell := false
	for _, character := range remark {
		if unicode.IsLetter(character) {
			// All yelling remarks have all uppercase letters
			if unicode.IsLower(character) {
				return false
			}
			mightYell = true
		}
	}

	return mightYell
}
