package raindrops

import "strconv"

// Convert takes the raindrop count and outputs what they sound like on a roof.
func Convert(input int) (response string) {
	if input%3 == 0 {
		response += "Pling"
	}

	if input%5 == 0 {
		response += "Plang"
	}

	if input%7 == 0 {
		response += "Plong"
	}

	if len(response) == 0 {
		response = strconv.Itoa(input)
	}

	return
}
